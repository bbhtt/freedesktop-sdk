kind: meson

build-depends:
- public-stacks/buildsystem-meson.bst
- bootstrap/diffutils.bst
- components/systemd.bst
- components/gtk-doc.bst
- components/gobject-introspection.bst
- components/cmake.bst
- components/polkit.bst
- components/python3-gi-docgen.bst
- components/python3-packaging.bst
- components/help2man.bst
- components/liberation-fonts.bst

depends:
- components/fwupd-efi-maybe.bst
- components/glib.bst
- components/libgudev.bst
- components/libxmlb.bst
- components/libjcat.bst
- components/libarchive.bst
- components/libcbor.bst
- components/gcab.bst
- components/efivar.bst
- components/tpm2-tss.bst
- components/cairo.bst
- components/pygobject.bst
- components/genpeimg.bst
- components/pango.bst
- components/polkit.bst
- components/pycairo.bst
- components/python3-pil.bst
- components/libsmbios-maybe.bst
- components/gnu-efi-maybe.bst

variables:
  plugin_msr: 'disabled'
  plugin_uefi_capsule: 'disabled'
  efi_binary: 'false'
  (?):
  - target_arch in ["aarch64", "loongarch64"]:
      plugin_uefi_capsule: 'enabled'
      efi_binary: 'true'
  - target_arch in ["i686", "x86_64"]:
      plugin_msr: 'enabled'
      plugin_uefi_capsule: 'enabled'
      efi_binary: 'true'

  # FIXME: consider adding dependencies for passim and all plugins
  meson-local: >-
    -Ddocs=enabled
    -Dplugin_msr=%{plugin_msr}
    -Dplugin_uefi_capsule=%{plugin_uefi_capsule}
    -Defi_binary=%{efi_binary}
    -Dpolkit=enabled
    -Dpassim=disabled
    -Dlaunchd=disabled
    -Dplugin_flashrom=disabled
    -Dplugin_amdgpu=disabled
    -Dplugin_logitech_bulkcontroller=disabled
    -Dplugin_modem_manager=disabled
    -Dvendor_ids_dir="%{datadir}/hwdata"
    -Dumockdev_tests=disabled
    -Dlibdrm=disabled

sources:
- kind: git_repo
  url: github:fwupd/fwupd.git
  track: '*.*.*'
  ref: 2.0.1-0-gf96cd611f92727bad2477199b154928117051e91
